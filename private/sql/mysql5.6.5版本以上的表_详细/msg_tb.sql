CREATE TABLE `msg_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id,消息id',
  `msg_title` VARCHAR(500) NOT NULL COMMENT '消息标题',
  `msg_content` VARCHAR(1000) NOT NULL COMMENT '消息内容',
  `receiver_user_id` INT UNSIGNED NOT NULL COMMENT '发信人用户id',
  `sender_user_id` INT UNSIGNED NOT NULL COMMENT '发信人用户id',
  `msg_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '消息状态,1=未读,2=已读',
  `send_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发送消息时间',
  `view_time` DATETIME NULL COMMENT '查看消息时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `receiver_userId_index` (`receiver_user_id`),
  INDEX `sender_userId_index` (`sender_user_id`),
  INDEX `msgStatus_index` (`msg_status`)
) ENGINE=MYISAM COMMENT='消息表' CHARSET=utf8 COLLATE=utf8_general_ci;