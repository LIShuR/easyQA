CREATE TABLE `relationship_group_tb`(
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `group_name` CHAR(24) NOT NULL COMMENT '组名',
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '收藏时间',
   PRIMARY KEY id_pk (`id`),
   INDEX userId_index (`user_id`),
   UNIQUE ids1_unique (`user_id`, `group_name`),
   INDEX addTime_index (`add_time`)
 )  ENGINE=MYISAM COMMENT='用户关系分组表' ROW_FORMAT=FIXED CHARSET=utf8;