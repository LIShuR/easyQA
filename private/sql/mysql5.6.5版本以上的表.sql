CREATE TABLE `article_append_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `append_content` TEXT NOT NULL COMMENT '追加内容',
  `append_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '追加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId` (`article_id`),
  INDEX `appendTime_index` (`append_time`)
) ENGINE=MYISAM COMMENT='文章追加表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `article_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `article_type` ENUM('1', '2', '3') NOT NULL COMMENT '文章类型,1=问答,2=讨论,3=头条',
  `article_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '文章状态,[提问类型时:1=未采纳,2=已采纳]',
  `article_title` VARCHAR(60) NOT NULL COMMENT '文章标题',
  `article_content` TEXT NOT NULL COMMENT '文章内容',
  `is_top` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '是否置顶1=未置顶,2=已置顶',
  `is_fine` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '是否精帖1=未加精,2=已加精',
  `view_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览数',
  `comment_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文章添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleType_index` (`article_type`),
  INDEX `articleStatus_index` (`article_status`),
  INDEX `isTop_index` (`is_top`),
  INDEX `isFine_index` (`is_fine`),
  INDEX `viewCounts_index` (`view_counts`),
  INDEX `commentCounts_index` (`comment_counts`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='文章表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `article_topic_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `topic_id` INT UNSIGNED NOT NULL COMMENT '话题id',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `ids1_uq` (`article_id`, `topic_id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `topicId_index` (`topic_id`)
) ENGINE=MYISAM COMMENT='文章话题表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `article_vote_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `vote_type` ENUM('1', '2') NOT NULL COMMENT '投票类型,1=顶 2=踩',
  `vote_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '投票时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `userId_index` (`user_id`),
  UNIQUE INDEX `ids1_uq` (`article_id`, `user_id`)
) ENGINE=MYISAM COMMENT='文章投票表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `comment_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `comment_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '评论状态,[文章为提问类型时:1=未采纳,2=已采纳]',
  `comment_content` TEXT NOT NULL COMMENT '评论内容',
  `reply_comment_id` INT UNSIGNED NULL COMMENT '被回复的评论id,null=非回复评论',
  `dialog_id` BIGINT UNSIGNED NULL COMMENT '对话id,当发生回复评论的时候生成对话id,方便查看对话,null=非对话',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `commentStatus_index` (`comment_status`),
  INDEX `replyCommentId_index` (`reply_comment_id`),
  INDEX `dialogId_index` (`dialog_id`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='评论表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `comment_vote_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `comment_id` INT UNSIGNED NOT NULL COMMENT '评论id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `vote_type` ENUM('1', '2') NOT NULL COMMENT '投票类型,1=顶 2=踩',
  `vote_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '投票时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `commentId_index` (`comment_id`),
  INDEX `userId_index` (`user_id`),
  UNIQUE INDEX `ids1_uq` (`comment_id`, `user_id`)
) ENGINE=MYISAM COMMENT='评论投票表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `github_user_tb`( 
   `openid` INT UNSIGNED NOT NULL COMMENT 'github用户ID', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(40) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用github登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用github登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='github登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `maopao_comment_vote_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `maopao_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `comment_id` INT UNSIGNED NOT NULL COMMENT '评论id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `vote_type` ENUM('1', '2') NOT NULL COMMENT '投票类型,1=顶 2=踩',
  `vote_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '投票时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `maopaoId_index` (`maopao_id`),
  INDEX `commentId_index` (`comment_id`),
  INDEX `userId_index` (`user_id`),
  UNIQUE INDEX `ids1_uq` (`comment_id`, `user_id`)
) ENGINE=MYISAM COMMENT='冒泡评论投票表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `maopao_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `maopao_content` VARCHAR(140) NOT NULL COMMENT '冒泡内容',
  `view_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览数',
  `comment_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '冒泡添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `viewCounts_index` (`view_counts`),
  INDEX `commentCounts_index` (`comment_counts`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='冒泡表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `maopao_vote_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `maopao_id` INT UNSIGNED NOT NULL COMMENT '冒泡id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `vote_type` ENUM('1', '2') NOT NULL COMMENT '投票类型,1=顶 2=踩',
  `vote_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '投票时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `maopaoId_index` (`maopao_id`),
  INDEX `userId_index` (`user_id`),
  UNIQUE INDEX `ids1_uq` (`maopao_id`, `user_id`)
) ENGINE=MYISAM COMMENT='冒泡投票表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `msg_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id,消息id',
  `msg_title` VARCHAR(500) NOT NULL COMMENT '消息标题',
  `msg_content` VARCHAR(1000) NOT NULL COMMENT '消息内容',
  `receiver_user_id` INT UNSIGNED NOT NULL COMMENT '发信人用户id',
  `sender_user_id` INT UNSIGNED NOT NULL COMMENT '发信人用户id',
  `msg_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '消息状态,1=未读,2=已读',
  `send_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发送消息时间',
  `view_time` DATETIME NULL COMMENT '查看消息时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `receiver_userId_index` (`receiver_user_id`),
  INDEX `sender_userId_index` (`sender_user_id`),
  INDEX `msgStatus_index` (`msg_status`)
) ENGINE=MYISAM COMMENT='消息表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `maopao_comment_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `maopao_id` INT UNSIGNED NOT NULL COMMENT '冒泡id',
  `comment_content` VARCHAR(140) NOT NULL COMMENT '评论内容',
  `reply_comment_id` INT UNSIGNED NULL COMMENT '被回复的评论id,null=非回复评论',
  `dialog_id` BIGINT UNSIGNED NULL COMMENT '对话id,当发生回复评论的时候生成对话id,方便查看对话,null=非对话',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `maopaoId_index` (`maopao_id`),
  INDEX `replyCommentId_index` (`reply_comment_id`),
  INDEX `dialogId_index` (`dialog_id`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='冒泡评论表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `oschina_user_tb`( 
   `openid` INT UNSIGNED NOT NULL COMMENT '开源中国用户ID', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(36) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用开源中国登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用开源中国登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='开源中国登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `qc_user_tb`( 
   `openid` CHAR(32) NOT NULL COMMENT '腾讯QQ用户openid', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(32) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用QQ登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用QQ登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='QQ登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `skin_setting_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
   `skin_id` INT UNSIGNED NOT NULL COMMENT '皮肤ID,自定义0',
   `lock_background` ENUM('1', '2') NOT NULL DEFAULT '2' COMMENT '锁定皮肤背景(背景图不随页面滚动),1=滚动,2=不滚动',
   `setting_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '皮肤设置时间',
   PRIMARY KEY `userId_pk` (`user_id`)
 ) ENGINE=MYISAM COMMENT='皮肤设置表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `skin_tb`( 
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '皮肤ID,行唯一ID', 
   `skin_name` VARCHAR(32) NOT NULL COMMENT '皮肤名称', 
   `skin_style` TEXT NOT NULL COMMENT '皮肤样式css',
   `skin_type` ENUM('1', '2') NOT NULL COMMENT '皮肤类型,1=静态,2=动态', 
   `skin_class` ENUM('1', '2', '3', '4', '5') NOT NULL COMMENT '皮肤分类,1=明星,2=动漫,3=萌宠,4=风景,5=其它', 
   `skin_stats` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '皮肤使用人数', 
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '皮肤添加时间',
   PRIMARY KEY `id_pk` (`id`),
   INDEX `skinType_index` (`skin_type`),
   INDEX `skinClass_index` (`skin_class`),
   INDEX `skinStats_index` (`skin_stats`),
   INDEX `addTime_index` (`add_time`)
 ) ENGINE=MYISAM COMMENT='皮肤表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `topic_tb`( 
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `topic` CHAR(20) NOT NULL COMMENT '话题',
  `used_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '话题使用次数',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '话题添加时间',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `topic_uq` (`topic`),
  INDEX `usedTimes_index` (`used_times`)
) ENGINE=MYISAM COMMENT='话题表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `user_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id,不能自动增长,需要与ucenter中的用户id保持一致',
  `email` CHAR(30) NULL COMMENT '用户注册邮箱',
  `mobile` CHAR(11) NULL COMMENT '用户手机',
  `pwd` CHAR(32) NULL COMMENT '密码',
  `nickname` CHAR(32) NOT NULL COMMENT '用户昵称,不能重复',
  `avatar_ext` CHAR(14) NULL COMMENT '头像图片扩展名,null=未上传头像(包括参数,防止图片缓存)',
  `gender` ENUM('n', 'f', 'm') NOT NULL DEFAULT 'n' COMMENT '性别,n保密,f女,m男',
  `country` ENUM('n', 'CN') NOT NULL DEFAULT 'n' COMMENT '国家,n保密',
  `province` ENUM('n') NOT NULL DEFAULT 'n' COMMENT '省份,n保密',
  `city` ENUM('n') NOT NULL DEFAULT 'n' COMMENT '城市,n保密',
  `birthday` DATE NULL COMMENT '生日',
  `sexual` ENUM('n', 'f', 'm', 'fm', 'nfm') NOT NULL DEFAULT 'n' COMMENT '性取向,n保密,f女,m男,fm双性,nfm无性',
  `relationship_status` ENUM('n', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NOT NULL DEFAULT 'n' COMMENT '感情状态,n保密,1单身,2求交往,3暗恋中,4暧昧中,5恋爱中,6订婚,7已婚,8分居,9离异,10丧偶',
  `blood_type` ENUM('n', 'A', 'B', 'AB', 'O') NOT NULL DEFAULT 'n' COMMENT '血型,n保密',
  `profession` ENUM('n') NOT NULL DEFAULT 'n' COMMENT '职业,n保密',
  `blog` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '博客地址',
  `brief` VARCHAR(140) NULL COMMENT '个人简介',
  `verify_type` ENUM('1', '2', '3', '4') NOT NULL DEFAULT '1' COMMENT '认证类型,1未认证,2网站认证,3微博黄V认证,4微博蓝V认证',
  `verify_details` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '认证详情',
  `verify_time` DATETIME NULL COMMENT '认证时间',
  `email_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '1邮箱未验证,2邮箱已验证',
  `mobile_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '1手机未验证,2手机已验证',
  `freeze_status` ENUM('1', '2') NOT NULL DEFAULT '1' COMMENT '1账号未冻结,2账号已冻结',
  `freeze_time` DATETIME NULL COMMENT '账号冻结时间',
  `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `signin_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最近登录时间',
  `signin_count` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `points` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户积分',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `email_uq` (`email`),
  UNIQUE INDEX `nickname_uq` (`nickname`),
  INDEX `points_index` (`points`)
) ENGINE=MYISAM COMMENT='用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `user_topic_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `topic_id` INT UNSIGNED NOT NULL COMMENT '话题id',
  `used_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '话题使用次数',
  `last_use_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '话题最后使用时间',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `ids1_uq` (`user_id`, `topic_id`),
  INDEX `lastUseTime_index` (`last_use_time`)
) ENGINE=MYISAM COMMENT='用户话题使用记录表,为了方便给用户展示最近使用过的或者经常使用的话题' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `weibo_user_tb`( 
   `openid` INT UNSIGNED NOT NULL COMMENT '微博用户ID', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(32) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用微博登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用微博登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='微博登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `weixin_user_tb`( 
   `unionid` CHAR(32) NOT NULL COMMENT '微信unionid',
   `openid` CHAR(32) NOT NULL COMMENT '微信openid',
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(128) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用微信登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用微信登录时间', 
   PRIMARY KEY `unionid_pk` (`unionid`),
   UNIQUE INDEX `openid_uq` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='微信登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `article_favorite_tb`(
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '收藏时间',
   PRIMARY KEY id_pk (`id`),
   INDEX userId_index (`user_id`),
   INDEX articleId_index (`article_id`)
 )  ENGINE=MYISAM COMMENT='文章收藏表' ROW_FORMAT=FIXED CHARSET=utf8;

CREATE TABLE `sign_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID', 
   `con_sign_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '连续签到次数', 
   `total_sign_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '总签到次数', 
   `sign_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '签到时间', 
   PRIMARY KEY userId_pk (`user_id`),
   INDEX conSignTimes_index (`con_sign_times`),
   INDEX totalSignTimes_index (`total_sign_times`),
   INDEX signTime_index (`sign_time`)
 )  ENGINE=MYISAM COMMENT='签到表' ROW_FORMAT=DEFAULT CHARSET=utf8;

CREATE TABLE `relationship_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `ruser_id` INT UNSIGNED NOT NULL COMMENT '关系用户ID',
   `remark` CHAR(32) NULL DEFAULT NULL COMMENT '备注名称',
   `group_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '分组id',
   `rtype` ENUM('1','2','3') NOT NULL COMMENT '关系类型,1=单向关注 2=互相关注 3=悄悄关注 4=黑名单',
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '关系时间',
   INDEX ruserId_index (`ruser_id`),
   UNIQUE ids1_uq (`user_id`, `ruser_id`),
   UNIQUE ids2_uq (`ruser_id`, `user_id`),
   INDEX groupId_index (`group_id`),
   INDEX addTime_index (`add_time`)
 )  ENGINE=MYISAM COMMENT='用户关系表' ROW_FORMAT=FIXED CHARSET=utf8;

CREATE TABLE `relationship_group_tb`(
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `group_name` CHAR(24) NOT NULL COMMENT '组名',
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '收藏时间',
   PRIMARY KEY id_pk (`id`),
   INDEX userId_index (`user_id`),
   UNIQUE ids1_unique (`user_id`, `group_name`),
   INDEX addTime_index (`add_time`)
 )  ENGINE=MYISAM COMMENT='用户关系分组表' ROW_FORMAT=FIXED CHARSET=utf8;