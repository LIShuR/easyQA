CREATE TABLE `comment_vote_tb`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `comment_id` INT UNSIGNED NOT NULL COMMENT '评论id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `vote_type` ENUM('1', '2') NOT NULL COMMENT '投票类型,1=顶 2=踩',
  `vote_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '投票时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `commentId_index` (`comment_id`),
  INDEX `userId_index` (`user_id`),
  UNIQUE INDEX `ids1_uq` (`comment_id`, `user_id`)
) ENGINE=MYISAM COMMENT='评论投票表' CHARSET=utf8 COLLATE=utf8_general_ci;